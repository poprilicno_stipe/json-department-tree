import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DataManipulationService } from './services/data-manipulation.service';
import { SideNavigationComponent } from './components/side-navigation/side-navigation.component';
import { MainViewComponent } from './components/main-view/main-view.component'


@NgModule({
  declarations: [
    AppComponent,
    SideNavigationComponent,
    MainViewComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [DataManipulationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
