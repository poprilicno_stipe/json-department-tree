import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { DataManipulationService } from 'src/app/services/data-manipulation.service';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css']
})
export class MainViewComponent implements OnInit, OnDestroy {
  showSelectedItem: any = [];
  subscription: Subscription;

  constructor(private dataManipulationService: DataManipulationService) {
    this.subscription = this.dataManipulationService.getData().subscribe(message => { this.showSelectedItem = message.data; });
  }

  
  ngOnInit() {}

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
