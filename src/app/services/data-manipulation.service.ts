import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataManipulationService {
  private subject = new Subject<any>();

  constructor() { }


  // transform flat array of objects to nested, tree-like one provided that every object has parent id property
  flatToTreeStructure(flatArray: any[], DepartmentParent_OID: number) {
    let treeStructure: any = []
    for (let i in flatArray) {
      if (flatArray[i].DepartmentParent_OID == DepartmentParent_OID) {
        let children: any = this.flatToTreeStructure(flatArray, flatArray[i].OID)

        if (children.length) {
          flatArray[i].children = children
        }
        treeStructure.push(flatArray[i])
      }
    }
    return treeStructure
  }

  sendData(data: Array<string>) {
    this.subject.next({ data });
  }

  getData(): Observable<any> {
    return this.subject.asObservable();
  }

}
