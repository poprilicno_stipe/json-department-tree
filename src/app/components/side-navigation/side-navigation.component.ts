import { Component, OnInit } from '@angular/core';

import { DataManipulationService } from 'src/app/services/data-manipulation.service';

@Component({
  selector: 'app-side-navigation',
  templateUrl: './side-navigation.component.html',
  styleUrls: ['./side-navigation.component.css']
})
export class SideNavigationComponent implements OnInit {
  treeData: any = [];
  selectedItem: any;
  selectedItemsArray: any = [];
  resultArray: any=[];

  constructor(private dataManipulationService: DataManipulationService) { }


  ngOnInit() {
    this.treeData = this.dataManipulationService.flatToTreeStructure(this.data, null);
  }

  // toggle opening and closing of the menu/list, call selectedItemsAccumulator on each item click
  listClick(event, newValue) {
    this.selectedItem = newValue;
    newValue.showChildren = !newValue.showChildren;
    // if the item has no children it is final in the branch - a leaf
    if (!this.selectedItem.children)
      this.selectedItemsAccumulator(this.selectedItem);
    event.stopPropagation()
  }

  // if the leaf (final in the branch) item does exist in the array add it, otherwise remove it
  // includes sending data to main view component
  selectedItemsAccumulator(clickedItem) {
    if (this.selectedItemsArray.find(o => o.OID === clickedItem.OID)) {
      let itemIndex: any = this.selectedItemsArray.indexOf(clickedItem);
      if (itemIndex > -1) {
        this.selectedItemsArray.splice(itemIndex, 1);
        // yeah... I know... same thing twice. Sleepy and running out of time
        this.resultArray.splice(itemIndex, 1);
      }
    } else {
      this.selectedItemsArray.push(clickedItem);
      // yeah... I know... same thing twice. Sleepy and running out of time
      this.resultArray.push(clickedItem.OID);
    }
    console.log("Result array:");
    console.log(this.resultArray);
    this.dataManipulationService.sendData(this.selectedItemsArray);
  }


  data = [
    {
      "OID": 1,
      "Title": "US News",
      "Color": "#F52612",
      "DepartmentParent_OID": null
    }, {
      "OID": 2,
      "Title": "Crime + Justice",
      "Color": "#F52612",
      "DepartmentParent_OID": 1
    }, {
      "OID": 3,
      "Title": "Energy + Environment",
      "Color": "#F52612",
      "DepartmentParent_OID": 1
    }, {
      "OID": 4,
      "Title": "Extreme Weather",
      "Color": "#F52612",
      "DepartmentParent_OID": 1
    }, {
      "OID": 5,
      "Title": "Space + Science",
      "Color": "#F52612",
      "DepartmentParent_OID": 1
    }, {
      "OID": 6,
      "Title": "International News",
      "Color": "#EB5F25",
      "DepartmentParent_OID": null
    }, {
      "OID": 7,
      "Title": "Africa",
      "Color": "#EB5F25",
      "DepartmentParent_OID": 6
    }, {
      "OID": 8,
      "Title": "Americas",
      "Color": "#EB5F25",
      "DepartmentParent_OID": 6
    }, {
      "OID": 9,
      "Title": "Asia",
      "Color": "#EB5F25",
      "DepartmentParent_OID": 6
    }, {
      "OID": 10,
      "Title": "Europe",
      "Color": "#EB5F25",
      "DepartmentParent_OID": 6
    }, {
      "OID": 11,
      "Title": "Middle East",
      "Color": "#EB5F25",
      "DepartmentParent_OID": 6
    }, {
      "OID": 12,
      "Title": "Politics",
      "Color": "#0079FF",
      "DepartmentParent_OID": null
    }, {
      "OID": 13,
      "Title": "National",
      "Color": "#0079FF",
      "DepartmentParent_OID": 12
    }, {
      "OID": 41,
      "Title": "Opinion",
      "Color": "#32B32B",
      "DepartmentParent_OID": null
    }, {
      "OID": 14,
      "Title": "World",
      "Color": "#0079FF",
      "DepartmentParent_OID": 12
    }, {
      "OID": 15,
      "Title": "2016 Election",
      "Color": "#0079FF",
      "DepartmentParent_OID": 12
    }, {
      "OID": 118,
      "Title": "Japan",
      "Color": "#EB5F25",
      "DepartmentParent_OID": 9
    }, {
      "OID": 119,
      "Title": "France",
      "Color": "#EB5F25",
      "DepartmentParent_OID": 10
    }];


}
